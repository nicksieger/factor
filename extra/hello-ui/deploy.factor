USING: tools.deploy.config ;
H{
    { deploy-math? t }
    { deploy-reflection 2 }
    { deploy-io 1 }
    { deploy-word-props? f }
    { deploy-word-defs? f }
    { "stop-after-last-window?" t }
    { deploy-ui? t }
    { deploy-compiler? t }
    { deploy-name "Hello world" }
    { deploy-c-types? f }
}
