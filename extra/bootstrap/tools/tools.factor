USING: vocabs.loader sequences ;

{
    "bootstrap.image"
    "tools.annotations"
    "tools.crossref"
    ! "tools.deploy"
    "tools.memory"
    "tools.profiler"
    "tools.test"
    "tools.time"
    "editors"
} [ require ] each
