USING: assocs kernel vectors sequences namespaces ;
IN: assocs.lib

: >set ( seq -- hash )
    [ dup ] H{ } map>assoc ;

: ref-at ( table key -- value ) swap at ;

: put-at* ( table key value -- ) swap rot set-at ;

: put-at ( table key value -- table ) swap pick set-at ;

: set-assoc-stack ( value key seq -- )
    dupd [ key? ] with find-last nip set-at ;

: at-default ( key assoc -- value/key )
    dupd at [ nip ] when* ;

: insert-at ( value key assoc -- )
    [ ?push ] change-at ;

: peek-at* ( key assoc -- obj ? )
    at* dup [ >r peek r> ] when ;

: peek-at ( key assoc -- obj )
    peek-at* drop ;

: >multi-assoc ( assoc -- new-assoc )
    [ 1vector ] assoc-map ;

: multi-assoc-each ( assoc quot -- )
    [ with each ] curry assoc-each ; inline

: insert ( value variable -- ) namespace insert-at ;
