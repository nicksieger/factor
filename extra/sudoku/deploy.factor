USING: tools.deploy.config ;
H{
    { deploy-reflection 2 }
    { deploy-word-props? f }
    { deploy-compiler? t }
    { deploy-math? f }
    { deploy-c-types? f }
    { deploy-io 2 }
    { deploy-ui? f }
    { deploy-name "Sudoku" }
    { "stop-after-last-window?" t }
    { deploy-word-defs? f }
}
