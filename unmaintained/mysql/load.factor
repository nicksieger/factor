! License: See http://factor.sf.net/license.txt for BSD license.
! Berlin Brown
! Date: 1/17/2007
!
! Adapted from mysql.h and mysql.c
! Tested with MySQL version - 5.0.24a
PROVIDE: libs/mysql
{ +files+ {
    "libmysql.factor"
    "mysql.factor"
} } ;