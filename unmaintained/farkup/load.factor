! Copyright (C) 2006 Matthew Willis. All Rights Reserved.
! See http://factorcode.org/license.txt for BSD license.
!
! Adapted from Wiky (http://goessner.net/articles/wiky/)
!
REQUIRES: libs/lazy-lists libs/parser-combinators ;

PROVIDE: libs/farkup
{ +files+ { 
  "farkup.factor"
  "farkup.facts"
} } ;